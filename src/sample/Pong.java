package sample;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static java.awt.Color.BLACK;


public class Pong extends JFrame {

    private static final  int width = 1000;
    private static final  int heigh = 400;

    private Dimension rozmiarEkranu = new Dimension(width, heigh);

    private Image image;
    private Graphics graphics;

    private static Ball kulka = new Ball(500, 200);


    public Pong() {
        this.setTitle("Pong");
        this.setSize(rozmiarEkranu);
        this.setResizable(false);
        this.setVisible(true);
        this.setBackground(BLACK);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addKeyListener(new AL());
    }

    @Override
    public void paint(Graphics g) {
        image = createImage(width, heigh);
        graphics = image.getGraphics();
        draw(graphics);
        g.drawImage(image, 0, 0, this);
    }


    private void draw(Graphics graphics) {
        kulka.draw(graphics);
        kulka.lewyGracz.draw(graphics);
        kulka.prawyGracz.draw(graphics);

        Font currentFont = graphics.getFont();
        Font newFont = currentFont.deriveFont(currentFont.getSize() * 10.0F);
        graphics.setFont(newFont);

        graphics.setColor(Color.WHITE);
        graphics.drawString("" + kulka.lewyPunkty, 200, 200);
        graphics.drawString("" + kulka.prawyPunkty, 700, 200);

        graphics.drawLine(width/2, 0, width/2, 60);
        graphics.drawLine(width/2, 110, width/2, 150);
        graphics.drawLine(width/2, 200, width/2, 240);
        graphics.drawLine(width/2, 280, width/2, 320);
        graphics.drawLine(width/2, 350, width/2, heigh);



        repaint();
    }


    public class AL extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            kulka.lewyGracz.keyPressed(e);
            kulka.prawyGracz.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            kulka.lewyGracz.keyReleased(e);
            kulka.prawyGracz.keyReleased(e);
        }
    }

    public static void main(String[] args) {
        Pong pong = new Pong();

        Thread ball = new Thread(kulka);
        ball.start();
        Thread LEWYGRACZ = new Thread(kulka.lewyGracz);
        Thread PRAWYGRACZ = new Thread(kulka.prawyGracz);
        LEWYGRACZ.start();
        PRAWYGRACZ.start();

    }
}
