package sample;

import java.awt.*;
import java.util.Random;

public class Ball implements Runnable{
    private int x;
    private int y;

    private int kierunekX;
    private int kierunekY;

    public Paletka lewyGracz = new Paletka(0, 160, 1);
    public Paletka prawyGracz = new Paletka(990, 160, 2);

    public Rectangle kulka;

    public int lewyPunkty;
    public int prawyPunkty;


    public Ball(int x, int y) {
        lewyPunkty = 0;
        prawyPunkty = 0;

        this.x = x;
        this.y = y;

        Random random = new Random();

        int randomowyKierunekX = random.nextInt(1);

        if(randomowyKierunekX == 0) {
            randomowyKierunekX--;
        }
        setXDirection(randomowyKierunekX);

        int randomowyKierunekY = random.nextInt(1);

        if(randomowyKierunekY == 0) {
            randomowyKierunekY--;
        }
        setYDirection(randomowyKierunekY);

        kulka = new Rectangle(this.x, this.y, 15, 15);


    }

    private void setYDirection(int randomowyKierunekY) {
        kierunekY = randomowyKierunekY;
    }

    private void setXDirection(int randomowyKierunekX) {
        kierunekX = randomowyKierunekX;
    }

    @Override
    public void run() {
        try {
            while(true) {
                move();
                Thread.sleep(8);
                //zatrzymuje na chwile zeby sprawdzic czy nie rzuci wyjatkiem
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void move() {
        zderzenia();
        kulka.x += kierunekX;
        kulka.y += kierunekY;

        if(kulka.x <= 0) {
            setXDirection(+1);
            prawyPunkty++;
        }

        if(kulka.x >= 990) {
            setXDirection(-1);
            lewyPunkty++;
        }

        if(kulka.y <= 15) {
            setYDirection(+1);
        }

        if(kulka.y >= 385) {
            setYDirection(-1);
        }

    }

    private void zderzenia() {
        if(kulka.intersects(lewyGracz.paletka)) {
            setXDirection(+1);
        }

        if(kulka.intersects(prawyGracz.paletka)) {
            setXDirection(-1);
        }
    }

    public void draw(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.fillRect(kulka.x, kulka.y, kulka.width, kulka.height);


    }
}
