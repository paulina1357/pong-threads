package sample;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Paletka implements Runnable{

    private int x;
    private int y;
    int kierunekY;
    private int id;

    public Rectangle paletka;

    public Paletka(int x, int y, int id) {
        this.x = x;
        this.y = y;
        this.id = id;
        paletka = new Rectangle(x, y, 10 ,80);
    }

    public void keyReleased(KeyEvent e) {
        switch(id) {
            default:
                System.out.println("Paletka");
                break;

            case 1:
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                    ruchKierunekY(0);
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    ruchKierunekY(0);
                }
                break;

            case 2:
                if (e.getKeyCode() == KeyEvent.VK_W) {
                    ruchKierunekY(0);
                    if (e.getKeyCode() == KeyEvent.VK_S) {
                        ruchKierunekY(0);
                    }
                    break;
                }
        }
    }

    public void keyPressed(KeyEvent e) {
        switch(id) {
            default:
                System.out.println("Paletka");
                break;

            case 1:
                if (e.getKeyCode() == KeyEvent.VK_W) {
                    ruchKierunekY(-1);
                }

                if (e.getKeyCode() == KeyEvent.VK_S) {
                    ruchKierunekY(1);
                }
                break;
            case 2:
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                    ruchKierunekY(-1);
                }

                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    ruchKierunekY(1);
                }
                break;
        }
    }

    private void ruchKierunekY(int kierunek) {
        kierunekY = kierunek;
    }

    @Override
    public void run() {
        try {
            while(true) {
                move();
                Thread.sleep(7);
                //zatrzymuje na chwile zeby sprawdzic czy nie rzuci wyjatkiem
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void move() {
        paletka.y += kierunekY;

        if(paletka.y <= 15) {
            paletka.y = 15;
        }

        if(paletka.y >= 340) {
            paletka.y = 340;
        }
    }

    public void draw(Graphics graphics) {
        switch(id) {
            default:
                System.out.println("Paletki");
            case 1:
                graphics.setColor(Color.WHITE);
                graphics.fillRect(paletka.x, paletka.y, paletka.width, paletka.height);
                break;
            case 2:
                graphics.setColor(Color.WHITE);
                graphics.fillRect(paletka.x, paletka.y, paletka.width, paletka.height);
                break;
        }
    }


}
